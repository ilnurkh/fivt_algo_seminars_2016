struct IFace {
    virtual void F1() = 0;
    virtual void F2() = 0;
    virtual void F3() = 0;
    virtual void F4() = 0;
    virtual void F5() = 0;
};

struct RealDummy : public IFace {
    void F1() override;
    void F2() override;
    void F3() override;
    void F4() override;
    void F5() override;
};


struct RealHard : public IFace {
    int c1 = 0;
    int c2 = 0;
    int c3 = 0;
    int c4 = 0;
    int c5 = 0;
    void F1() override;
    void F2() override;
    void F3() override;
    void F4() override;
    void F5() override;
};

struct Mem {
    static int count1;
    static int count2;
    static int count3;
    static int count4;
    static int count5;
};

void RunF1(IFace&);
void RunF2(IFace&);
void RunF3(IFace&);
void RunF4(IFace&);
void RunF5(IFace&);
