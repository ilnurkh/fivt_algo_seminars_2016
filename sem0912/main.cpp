#include <iostream>
#include <memory>
#include <strstream>

#include "iface.h"

using namespace std;

int main(int arc, char* argv[]) {
    std::unique_ptr<IFace> obj;
    if (argv[1][0] == 'd') {
        obj.reset(new RealDummy);
    } else {
        obj.reset(new RealHard);
    }
    //std::string nums(argv[2]);
    //std::strstream stream(argv[2], nums.size());
    
    int num = 0;
    cin >> num;
    //cout << nums << " " << num << endl;
    for (int i = 0; i < 10 * num; ++i) {
        RunF1(*obj);
        RunF2(*obj);
        RunF3(*obj);
        RunF4(*obj);
        RunF5(*obj);
    }

    cout << Mem::count1 << endl;
    cout << Mem::count2 << endl;
    cout << Mem::count3 << endl;
    cout << Mem::count4 << endl;
    cout << Mem::count5 << endl;
}
